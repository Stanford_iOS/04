//
//  Array+Only.swift
//  StanfordiOS
//
//  Created by Henderson on 2020/09/18.
//  Copyright © 2020 Henderson. All rights reserved.
//

import Foundation

extension Array {
    var only: Element? {
        count == 1 ? first : nil
    }
}
