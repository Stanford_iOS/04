//
//  Array+Identifiable.swift
//  StanfordiOS
//
//  Created by Henderson on 2020/09/17.
//  Copyright © 2020 Henderson. All rights reserved.
//

import Foundation

extension Array where Element: Identifiable {
    func firstIndex(matching: Element) -> Int? {
        for index in 0..<self.count {
            if self[index].id == matching.id {
                return index
            }
        }
        return nil
    }
}
